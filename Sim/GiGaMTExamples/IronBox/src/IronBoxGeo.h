/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb and FCC Collaborations  *
*                                                                             *
* This software is distributed under the terms of the Apache License          *
* version 2 (Apache-2.0), copied verbatim in the file "COPYING".              *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// from Gaudi
#include "GaudiKernel/Kernel.h"
#include "GaudiKernel/Service.h"
#include "GaudiKernel/StatusCode.h"

// from GiGa
#include "GiGaMTGeo/IGiGaMTGeoSvc.h"

// Forwad declarations
//template <class TYPE>
//class SvcFactory;

// from G4
class G4VPhysicalVolume;

/**  @class IronBoxGeo IronBoxGeo.h
 *
 *   Simple geometry service that constructs an iron box.
 *
 *    @author: Dominik Muller
 */

class IronBoxGeo : public Service, virtual public IGiGaMTGeoSvc
{

  /// friend factory
  //friend class SvcFactory<IronBoxGeo>;

protected:
  using Service::Service;

public:
  StatusCode initialize() override {return Service::initialize();}
  StatusCode finalize() override {return Service::finalize();}


  virtual G4VPhysicalVolume* constructWorld() override;
  virtual void constructSDandField() override {};
  virtual StatusCode queryInterface( const InterfaceID& iid, void** pI ) override;
};
