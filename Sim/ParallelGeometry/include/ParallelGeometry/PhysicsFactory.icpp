/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb and FCC Collaborations  *
*                                                                             *
* This software is distributed under the terms of the Apache License          *
* version 2 (Apache-2.0), copied verbatim in the file "COPYING".              *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

template <class ParallelPhysics>
ParallelPhysics* ParallelGeometry::PhysicsFactory<ParallelPhysics>::construct() const {
  debug() << "Constructing parallel physics: " << name() << endmsg;
  auto physics = new Physics{m_worldName.value(), m_layeredMass.value()};

  physics->SetVerboseLevel( verbosity() );
  physics->SetMessageInterface( message_interface() );
  physics->setParticlePIDs( m_particlePIDs.value() );
  physics->setProcessConstructor( [&]() {
    debug() << "Constructing a process constructor for " << name() << " parallel physics" << endmsg;

    // additional implementation
    return additionalProcessConstructor();
  } );

  debug() << "Constructed fast simulation physics:" << name() << endmsg;
  return physics;
}
