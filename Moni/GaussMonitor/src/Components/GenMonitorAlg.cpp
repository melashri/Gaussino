/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb and FCC Collaborations  *
*                                                                             *
* This software is distributed under the terms of the Apache License          *
* version 2 (Apache-2.0), copied verbatim in the file "COPYING".              *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// $Id: GenMonitorAlg.cpp,v 1.16 2010-02-24 19:02:33 robbep Exp $
// Include files

// from Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// From HepMC
#include "HepMC3/GenEvent.h"
#include "HepMC3/GenParticle.h"
#include "HepMC3/GenVertex.h"

// From LHCb
#include "Kernel/ParticleID.h"

// local
#include "GaussGenUtil.h"
#include "GenMonitorAlg.h"

// Get the default names for the HepMC attributes
#include "Defaults/HepMCAttributes.h"

//-----------------------------------------------------------------------------
// Implementation file for class : GenMonitorAlg
//
// 2003-11-13 : Patrick Robbe
// 2005-04-11 : G.Corti
// 2018-03-08 : D. Muller
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( GenMonitorAlg )

StatusCode GenMonitorAlg::initialize() {

  StatusCode sc = GaudiHistoAlg::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;             // error printed already by GaudiHistoAlg

  debug() << "==> Initialize" << endmsg;

  if ( m_generatorName.empty() ) {
    info() << "Monitor will be applied to events produced with generator " << m_generatorName << endmsg;
  }

  if ( produceHistos() ) { bookHistos(); }

  return StatusCode::SUCCESS;
}

void GenMonitorAlg::operator()( const HepMC3::GenEventPtrs& hepmcevents ) const {
  std::lock_guard<std::mutex> lock( m_histo_lock );
  debug() << "==> Execute" << endmsg;

  // Initialize counters
  int nParticles( 0 ), nParticlesStable( 0 );
  int nParticlesStableCharged( 0 ), nParChStabEtaAcc( 0 );
  int nPileUp( 0 );

  for ( auto& hepmcevent : hepmcevents ) {
    auto gen_name =
        hepmcevent->attribute<HepMC3::StringAttribute>( Gaussino::HepMC::Attributes::GeneratorName )->value();

    // Check if monitor has to be applied to this event
    if ( !m_generatorName.empty() ) {
      if ( m_generatorName != gen_name ) { continue; }
    }
    debug() << "Monitor for " << gen_name << endmsg;

    // Get the signal process ID from the attributes
    if ( produceHistos() ) {
      auto sig_proc_id_attr =
          hepmcevent->attribute<HepMC3::IntAttribute>( Gaussino::HepMC::Attributes::SignalProcessID );
      if ( sig_proc_id_attr ) {
        auto sig_proc_id = sig_proc_id_attr->value();
        m_hProcess->fill( sig_proc_id );
      }
    }

    bool primFound = false;
    nPileUp++;
    for ( auto& hepMCpart : hepmcevent->particles() ) {
      nParticles++;
      if ( produceHistos() ) {
        // Identify primary vertex and fill histograms
        if ( !primFound ) {
          if ( ( hepMCpart->status() == 1 ) || ( hepMCpart->status() == 888 ) ) {
            primFound = true;
            if ( hepMCpart->production_vertex() ) {
              m_hPrimX->fill( hepMCpart->production_vertex()->position().x() / Gaudi::Units::mm );
              m_hPrimY->fill( hepMCpart->production_vertex()->position().y() / Gaudi::Units::mm );
              m_hPrimZ->fill( hepMCpart->production_vertex()->position().z() / Gaudi::Units::mm );
              m_hPrimZV->fill( hepMCpart->production_vertex()->position().z() / Gaudi::Units::mm );
              m_hPrimZE->fill( hepMCpart->production_vertex()->position().z() / Gaudi::Units::mm );
              m_hPrimT->fill( hepMCpart->production_vertex()->position().t() / Gaudi::Units::ns );
              m_hPrimXvsZ->fill( hepMCpart->production_vertex()->position().z() / Gaudi::Units::mm,
                                 hepMCpart->production_vertex()->position().x() / Gaudi::Units::mm );
              m_hPrimYvsZ->fill( hepMCpart->production_vertex()->position().z() / Gaudi::Units::mm,
                                 hepMCpart->production_vertex()->position().y() / Gaudi::Units::mm );
            }
          }
        }

        m_hPartP->fill( hepMCpart->momentum().p3mod() / Gaudi::Units::GeV );
        m_hPartPDG->fill( hepMCpart->pdg_id() );
      }
      // Note that the following is really multiplicity of particle defined
      // as stable by Pythia just after hadronization: all particles known by
      // EvtGen are defined stable for Pythia (it will count rho and pi0
      // and gamma all togheter as stable ...
      if ( ( hepMCpart->status() != 2 ) && ( hepMCpart->status() != 3 ) ) {
        nParticlesStable++;
        if ( produceHistos() ) {
          m_hProtoP->fill( hepMCpart->momentum().p3mod() / Gaudi::Units::GeV );
          m_hProtoPDG->fill( hepMCpart->pdg_id() );
          m_hProtoLTime->fill( GaussGenUtil::lifetime( hepMCpart ) / Gaudi::Units::mm );
        }
        // Charged stable particles meaning really stable after EvtGen
        LHCb::ParticleID pID( hepMCpart->pdg_id() );
        if ( 0.0 != pID.threeCharge() ) {
          // A stable particle does not have an outgoing vertex
          if ( !hepMCpart->end_vertex() ) {
            // should be the same as the following
            //             if ( ( hepMCpart -> status() == 999 )
            ++nParticlesStableCharged;
            double pseudoRap = hepMCpart->momentum().pseudoRapidity();
            // in LHCb acceptance
            if ( ( pseudoRap > m_minEta ) && ( pseudoRap < m_maxEta ) ) { ++nParChStabEtaAcc; }
            if ( produceHistos() ) {
              m_hStableEta->fill( pseudoRap );
              m_hStablePt->fill( hepMCpart->momentum().perp() / Gaudi::Units::GeV );
            }
          }
        }
      }
    }
  }

  if ( produceHistos() ) {
    m_hNPart->fill( nParticles );
    m_hNStable->fill( nParticlesStable );
    m_hNSCharg->fill( nParticlesStableCharged );
    m_hNSChEta->fill( nParChStabEtaAcc );
    m_hNPileUp->fill( nPileUp );
  }
  m_counter += nParticles;
  m_counterstable += nParticlesStable;
  m_counterCharged += nParticlesStableCharged;
  m_counterChInEta += nParChStabEtaAcc;
  m_nEvents++;

  debug() << "Event number " << m_nEvents << " contains " << nParticles << " particles" << endmsg;
}

//=============================================================================
//  Finalize
//=============================================================================
StatusCode GenMonitorAlg::finalize() {

  debug() << "==> Finalize" << endmsg;

  info() << std::endl
         << "======================== Generators Statistics ====================" << std::endl
         << "=                                                                 =" << std::endl
         << "= Number of particles generated: " << m_counter << std::endl
         << "= Number of events: " << m_nEvents << std::endl
         << "= Mean multiplicity: " << m_counter / (double)m_nEvents << std::endl
         << "=                                                                 =" << std::endl
         << "= Number of pseudo stable particles generated: " << m_counterstable << std::endl
         << "= Number of events: " << m_nEvents << std::endl
         << "= Mean pseudo stable multiplicity: " << m_counterstable / (double)m_nEvents << std::endl
         << "=                                                                 =" << std::endl
         << "= Number of charged stable particles generated: " << m_counterCharged << std::endl
         << "= Number of events: " << m_nEvents << std::endl
         << "= Mean charged stable multiplicity: " << m_counterCharged / (double)m_nEvents << std::endl
         << "=                                                                 =" << std::endl
         << "= Number of charged stable particles in LHCb eta " << m_counterChInEta << std::endl
         << "= Number of events: " << m_nEvents << std::endl
         << "= Mean charged stable multiplicity in LHCb eta: " << m_counterChInEta / (double)m_nEvents << std::endl
         << "=                                                                 =" << std::endl
         << "===================================================================" << endmsg;

  return GaudiHistoAlg::finalize();
}

//============================================================================
// Booking of histograms
//============================================================================
void GenMonitorAlg::bookHistos() {

  debug() << "==> Book histograms" << endmsg;

  m_hNPart      = book( 1, "Multiplicity all particles", 0., 2999., 300 );
  m_hNStable    = book( 2, "Multiplicity protostable particles", 0., 2999., 300 );
  m_hNSCharg    = book( 3, "Multiplicity stable charged particles", -0.5, 299.5, 300 );
  m_hNSChEta    = book( 4, "Multiplicity stable charged particles in LHCb eta", -0.5, 299.5, 300 );
  m_hProcess    = book( 5, "Process type", -0.5, 5100.5, 5101 );
  m_hNPileUp    = book( 10, "Num. of primary interaction per bunch", -0.5, 10.5, 11 );
  m_hPrimX      = book( 11, "PrimaryVertex x (mm)", -1.0, 1.0, 200 );
  m_hPrimY      = book( 12, "PrimaryVertex y (mm)", -1.0, 1.0, 200 );
  m_hPrimZ      = book( 13, "PrimaryVertex z (mm)", -200., 200., 100 );
  m_hPrimZV     = book( 14, "PrimaryVertex z, all Velo (mm)", -1000., 1000., 100 );
  m_hPrimZE     = book( 15, "PrimaryVertex z, all Exp Area (mm)", -1000., 20000., 105 );
  m_hPrimT      = book( 16, "PrimaryVertex t (ns)", -75.5, 75.5, 151 );
  m_hPrimXvsZ   = book2D( 17, "PrimaryVertex x vs z (mm)", -500., 500., 100, -1., 1., 100 );
  m_hPrimYvsZ   = book2D( 18, "PrimaryVertex y vs z (mm)", -500., 500., 100, -1., 1., 100 );
  m_hPartP      = book( 21, "Momentum of all particles (GeV)", 0., 100., 100 );
  m_hPartPDG    = book( 22, "PDGid of all particles", -4999., 5000., 10000 );
  m_hProtoP     = book( 31, "Momentum of protostable particles (GeV)", 0., 100., 100 );
  m_hProtoPDG   = book( 32, "PDGid of protostable particles", -4999., 5000., 10000 );
  m_hProtoLTime = book( 33, "Lifetime protostable particles (mm) ", -1., 20., 105 );
  m_hStableEta  = book( 44, "Pseudorapidity stable charged particles", -15., 15., 150 );
  m_hStablePt   = book( 45, "Pt stable charged particles", 0., 20., 100 );

  return;
}
