# Further reading

You can find more information about Gaussino in the following papers:

```{eval-rst}
.. [GaussinoPaper1] D. Muller and B. G. Siddi, `Gaussino - a Gaudi-Based Core Simulation Framework <https://ieeexplore.ieee.org/document/9060074>`_,

.. [GaussinoPaper2] D. Muller, `Adopting new technologies in the LHCb Gauss simulation framework <https://www.epj-conferences.org/articles/epjconf/abs/2019/19/epjconf_chep2018_02004/epjconf_chep2018_02004.html>`_

.. [GaussinoPaper3] M. Mazurek, G.Corti, D. Muller, `New Simulation Software Technologies at the LHCb Experiment at CERN <http://www.cai.sk/ojs/index.php/cai/article/view/2021_4_815>`_
```
